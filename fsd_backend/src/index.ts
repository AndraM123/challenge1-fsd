import "reflect-metadata";
import {createConnection, QueryResult} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response,NextFunction} from "express";
import {Routes} from "./routes";
import {Users} from "./entity/User";

createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());
    var router = express.Router();

    router.use("/", (request: Request, response: Response, next: NextFunction) => {
        console.log(`[${request.method}]: ${request.originalUrl}`);
        next();
    });

    // register express routes from defined application routes
    Routes.forEach((route) => {
        if (route.middlewares) {
            route.middlewares.forEach((middleware) => {
                (router as any)[route.method](route.route, middleware);
            });
        }
        (router as any)[route.method](
            route.route,
            (request: Request, response: Response, next: NextFunction) => {
                const result = new (route.controller as any)()[route.action](request, response, next);
                if (result instanceof Promise) {
                    result.then((result) => 
                        result !== null && result !== undefined
                        ? response.send(result)
                        : undefined);
                } else if (result !== null && result !== undefined) {
                    response.json(result);
                }
            }
        );
    });

    app.use("/", router);

    const result = require("dotenv").config();

    if (result.error) {
        throw result.error;
    }

    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    // insert new users for test
    // await connection.manager.save(connection.manager.create(Users, {
    //     email: "flavius@email.com",
    //     username: "Flavius",
    //     password: "1234"
    // }));

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));
