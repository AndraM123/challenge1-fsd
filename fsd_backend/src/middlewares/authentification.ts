import { NextFunction, Request, Response} from "express";

var jwt = require("jsonwebtoken");

export function authenticate(request: Request, response: Response, next: NextFunction) {
    try {
        let token: String = request.headers.token;
        if (!jwt.verify(token, process.env.TOKEN_SECRET)) {
            response.sendStatus(401);
            return;
        }
    } catch (error) {
        console.log("Invalid token");
        response.sendStatus(401);
        return;
    }
    return next();
}