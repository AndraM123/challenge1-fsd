import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Component } from "react";

const theme = createTheme();

export const theme_bac = {
  background: "linear-gradient(45deg,#00ccff, #ff6699)",
  height: "100vh",
  width: "100vw",
};

class App extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.buttonClicked = this.buttonClicked.bind(this);
    this.state = {
      username: "",
      password: "",
    };
  }

  setUsername = (event: any) => {
    this.setState({
      username: event.target.value,
    });
  };

  setPassword = (event: any) => {
    this.setState({
      password: event.target.value,
    });
  };

  buttonClicked = () => {
    let flag = false;
    let true_email = "admin@yahoo.com";
    let true_passwords = "admin";
    if (
      this.state.username === true_email &&
      this.state.password === true_passwords
    )
      flag = true;
    if (flag === false) alert("Wrong credentials!");
    else this.props.history.push("/table");
  };

  render() {
    const handleSubmit = (event: {
      preventDefault: () => void;
      currentTarget: HTMLFormElement | undefined;
    }) => {
      event.preventDefault();
      const data = new FormData(event.currentTarget);
      // eslint-disable-next-line no-console
      console.log({
        email: data.get("email"),
        password: data.get("password"),
      });
    };

    return (
      <ThemeProvider theme={theme}>
        <Box
          maxWidth={"100%"}
          maxHeight={"100%"}
          height={"937px"}
          style={{ ...theme_bac }}
        >
          <Grid container component="main">
            <Grid
              item
              xs={12}
              sm={11}
              md={6}
              component={Paper}
              square
              marginTop={25}
              marginLeft={59.75}
              borderRadius={5}
            >
              <Box
                sx={{
                  mx: 18,
                  mt: 18,
                  display: "flex",
                  // flexDirection: 'column',
                  alignItems: "center",
                }}
              >
                <Avatar sx={{ width: 140, height: 140, m: 1 }}></Avatar>
              </Box>

              <Box
                sx={{
                  ml: 55,
                  mr: 20,
                  mt: -25,
                  mb: 10,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "right",
                }}
              >
                <Typography component="h1" variant="h5" align="center">
                  User Login
                </Typography>
                <Box
                  component="form"
                  noValidate
                  onSubmit={handleSubmit}
                  sx={{ mt: 1 }}
                >
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    onChange={this.setUsername}
                    value={this.state.username}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={this.setPassword}
                    value={this.state.password}
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="success"
                    sx={{ mt: 3, mb: 2 }}
                    onClick={this.buttonClicked}
                  >
                    Login
                  </Button>
                  <Grid container>
                    <Grid item xs>
                      <Box
                        sx={{
                          mx: 4,
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                        }}
                      >
                        <Link href="#" variant="body2" underline="none">
                          Forgot Username /Password?
                        </Link>
                      </Box>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Box>
      </ThemeProvider>
    );
  }
}
export default App;
