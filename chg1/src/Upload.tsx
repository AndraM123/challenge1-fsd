import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

const FileInput = () => {
  return (
    <Box bgcolor={"#cccccc"} width={"900px"} height={"700px"} mt={15} ml={60}>
      <input
        accept="image/*"
        type="file"
        id="select-image"
        style={{ display: "none" }}
        //onChange={e => setSelectedImage(e.target.files[0])}
      />
      <label htmlFor="select-image">
        <Button
          variant="contained"
          color="default"
          component="span"
          style={{ marginLeft: "570px", marginTop: "25px" }}
        >
          Upload Image
        </Button>
      </label>
      <Box>
        <Box
          mt={5}
          ml={22}
          textAlign="center"
          bgcolor={"#eeeeee"}
          width={"550px"}
          height={"550px"}
        >
          <div style={{ paddingTop: "250px" }}>Image:</div>
          <img alt="" height="100px" />
        </Box>
      </Box>
    </Box>
  );
};

export default FileInput;
