import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Table from "./Table";
import Upload from "./Upload";
import DataPicker from "./DataPicker"
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Switch, Route } from "react-router-dom";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={DataPicker}></Route>
      <Route path="/table" component={Table}></Route>
      <Route path="/upload" component={Upload}></Route>
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);

reportWebVitals();
