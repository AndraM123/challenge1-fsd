import * as React from "react";
import "./DataPicker.scss"
import { useState } from "react";

import Box from "@mui/material/Box";
 const DataPicker = () => {
        const [isHidden, setIsHidden] = useState(false);

        return (
            <div className="ansamblu">
                      <div className="typeDate" onClick={() => setIsHidden(!isHidden)}>
                        <input min="2022-02-01" type="date" />
                        <input max="2022-02-28" type ="date" />
                      </div>
                    <main className={isHidden ? 'middle' : 'middle hidden'}>
                        <div className="calendar">
                            <div className="month-indicator">
                                <Box>
                                <time dateTime="15"> 15 </time>

                                <time dateTime="02"> February </time>

                                <time dateTime="2022"> 2022 </time> 
                                </Box>
                            </div>
                            <div className="day-of-week">
                                <div>Su</div>
                                <div>Mo</div>
                                <div>Tu</div>
                                <div>We</div>
                                <div>Th</div>
                                <div>Fr</div>
                                <div>Sa</div>
                            </div>
                            <div className="date-grid">
                                <button>
                                    <time dateTime="2022-02-01">1</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-02">2</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-03">3</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-04">4</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-05">5</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-06">6</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-07">7</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-08">8</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-09">9</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-10">10</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-11">11</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-12">12</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-13">13</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-14">14</time>
                                </button>
                                <button autoFocus >
                                    <time dateTime="2022-02-15">15</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-16">16</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-17">17</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-18">18</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-19">19</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-20">20</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-21">21</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-22">22</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-23">23</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-24">24</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-25">25</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-26">26</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-27">27</time>
                                </button>
                                <button>
                                    <time dateTime="2022-02-28">28</time>
                                </button>
                            </div>
                        </div>
                        <div className = "buttom">
                            <button>Cancel</button>
                            <button>Done</button>
                        </div>
                    </main>
                </div>

        )
    }

    export default DataPicker;